# Rocketship theme extention

This module is an example and POC for how to add theme extentions for Rocketship themes using modules.

## Generate theme extentions

Some Rocketship modules add new functionality (eg. new Content Types or Block Types). For ease of reference, let's call those 'Features'.<br />
If those new Features need extensive styling, there's a good chance the module already contains a bunch of theming assets (eg. Sass files or JS) with the purpose of being copied over to the themes themselves (because that's where theming happens).<br />
This happens automagically when the module is installed, if it has a hook_install() function connecting it to the theme generator.

This install function runs during module install, which calls the ThemeExtentionGenerator class of the theme generator, which copies the appropriate files over to ALL the themes.<br />
Why all the themes? Because if the module is activated during a site installation, there might not be an active theme yet. So we target all of them to include the styling that is needed for the Feature to look decent.<br />
By default, the styling will be either really bare-bones, or based on the Demo theme, because that looks the best. Of course, when you start theming your project, you'll have to change things to make them match whatever design you are working from.

If you make and add a new theme after the module was installed, you'll have to copy those files from the Feature's module manually. They should be located in `my_module/assets/theming` and copied or merged with your new theme.<br />

**Note: not all Rocketship modules that add new functionality have these theming assets, in which case, you need to override whatever theming and libraries yourself, to get the styling you want.**

**Note 2: as it stands, extending theming in this way is NOT compatible with child themes. This is because of the way globbing the Sass-files works, where you need to know the name of your parent theme so this is not generic the way we need it.**

**Note 3: your themes need to be located in `/themes/custom`, on level 0, for the generator to put the files in the correct place**

### Developing a theme extention

There are 2 ways to do this:
- make an entirely separate component
- extend or add components to the `00-theme` or `01-content-blocks` folders of the theme

**1) Separate component**

_recommended for new functionality_

First, you need to make the Rocketship Theme Generator a dependency and tell it is okay to handle the theming files for your module. <br />
This is done by adding a dependency to the info yml, and by triggering a custom hook from your modul's install file:

Inside `my_module.info.yml`

```
dependencies:
  - rocketship_theme_generator
```

Inside `my_module.module`

```
function my_module_install() {

  // need to pass the module name so we know where stuff needs to get copied from
  $args = array(
    'module_name' => 'my_module'
  );

  // Find the hook that copies the theming from this module into the themes
  // (should only be 1 such hook, and it is in rocketship_theme_generator)
  \Drupal::moduleHandler()->invokeAll('generate_theme_extention', $args);

}
```

Then you would put your theming into a self-contained folder, nested in your theme's `components/02-features`, while developing.<br />
because any folders inside 'features' will be compiled by Gulp's CSS tasks without additional development needed in the theme.<br />
When finished, you copy all your component files & folders to your module's `assets/theming` folder.</br>
So for example: `your_theme/components/02-features/my-component` ends up in `assets/theming/components/02-features/my-component`.

Don't forget to include all the library files (generated CSS and JS) into your module's `assets/theming/css`and `assets/theming/js/dest` folders.
Your module also needs to have a snippet file called `assets/theming/libraries.yml` containing the library definition. Otherwise, there is no way to actually load your styling out-of-the-box.

Example snippet in `libraries.yml`
```
feature_my_component:
  css:
    theme:
      css/style.my-component.css: {}
      css/style.my-component.print.css: {}
  js:
    js/dest/my-component.js: {}
  dependencies:
    - core/jquery
    - core/drupal
    - core/jquery.once
```

Now you need to make sure your library gets activated when the module is installed, by using your module's `.module` file, eg. by adding a `HOOK_preprocess_HOOK` function.

```
function my_module_preprocess_page(&$variables) {
  // find active theme name
  $active_theme = \Drupal::service('theme.manager')->getActiveTheme()->getName();
  // attach component library
  $variables['#attached']['library'][] = $active_theme . '/feature_my_new_component';
```

**In short:<br />**
You set up your module's install function to trigger a custom hook.<br />
Once your module's install function has run, the component and the library files end up copied to the theme<br />
and your theme library yml will be extended with the library definition.<br/>
The new library gets loaded by the module itself, pointing to the active theme. So even if no further front-end development is happening, your new component styling is loaded and the component looks good & works in the front-end.

Uninstalling the module will NOT remove the component assets from your theme. This is because themes are custom parts of a Drupal project and subject to further changes and development. Having a module remove parts of it could break the theme.<br />
However, uninstalling the module WILL prevent the library from being active on your project, so there won't be any extra load to the pages.

**2) Adding to 00-theme or 01-content-blocks**

_recommended ONLY if still developing the front-end of the project_

Same setup as before, and you also add add your files to your module's `assets/theming` folder.<br />
Eg. if you want to add a new molecule to content-blocks, you place files in:<br />
`assets/theming/components/00-theme/01-atoms/my-new-atom`<br />
You can't extend or modify existing files (because that could break existing theming).

However, it is no use adding a new library snippet nor library files (CSS and JS files) for your extra styling, because `00-theme` and content blocks already have single libraries and CSS files that are attached globally. Any new files you add to those folders will already automagically be picked up by Gulp and compiled into those global libraries during theming of the site.

While you COULD add a new, separate library, it is useless to have compiled CSS and JS be loaded when you turn on the module. Once you would start making theming changes, your new styling would get loaded twice: updated theming in the normal theming CSS and the old theming that never gets recompiled, via a separate library.

So this way this type of extending a theme is only useful if you know front-end development (and thus CSS Gulp tasks) will happen AFTER your module has been turned on and the theming doesn't have to be visible out-of-the-box. It's useless for demo-projects without development.

### Customizing component theming

Once your theme extention lives in the theme you are working on, you can simply update the CSS and JS by using the normal Gulp tasks, such as:

- `gulp css:features:prod`: if you only want to update the Feature css
- etc…
